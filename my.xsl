﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/tei/text/body">
    <br/>
    <br/>
    <xsl:for-each select="entry">
      <span>
        <xsl:value-of select="form"/>
      </span>
      <span style="color:grey;">
        <xsl:value-of select="root"/>
      </span>
      <span style="color:red;">
        <xsl:value-of select="group"/>
      </span>
      <span style="color:blue;">
        <xsl:value-of select="xr"/>
      </span>
      <br/>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
